# Cinetixx API Client Library

## Changelog:

* v2.0: Initial release

    Client implementing the Cinetixx XML API: https://api.cinetixx.de/

* v2.1: YoutubeId release

    Youtube ID will be calculated from trailer link

* v2.2: Autokino release

    Respect the location and extract correct Kinoheld Links

## Todo

* Request seat infos

